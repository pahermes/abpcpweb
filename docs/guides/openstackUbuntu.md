# Virtual Machine Ubuntu 20.04

Using the Openstack command line interface it is possible to access a larger number of images, including Ubuntu ones (these are not officially supported by IT, but are available).

The following is largely based on these documentation pages:

 - https://clouddocs.web.cern.ch/tutorial/index.html
 - https://clouddocs.web.cern.ch/containers/training.html#cern

The openstack command line interface is available on lxplus8.cern.ch.

```bash
ssh lxplus8.cern.ch
```

## Create the machine

On lxplus8, by default you will work on your personal OpenStack space. You can switch to a given openstack project by running the script downloadable from the first link
```
source openstack_sixtrack.sh
```

To list the available images and see the ubuntu ones:
```
openstack image list --community | grep -i ubuntu
```

Create an rsa key pair and provide it to openstack:

```
ssh-keygen -t rsa
openstack keypair delete lxplus # I want to start clean (but probably not needed)
openstack keypair create --public-key ~/.ssh/id_rsa.pub lxplus
```

To correctly configure the DNS you need to create the following config file:

```
vim ubuntu-systemd-resolved.yaml
```
Type:
```
#cloud-config
bootcmd:
- printf "[Resolve]\nDNS=137.138.16.5 137.138.17.5\n" > /etc/systemd/resolved.conf
- [systemctl, restart, systemd-resolved]
```

Create the virtual machine:
```
openstack server create --user-data ubuntu-systemd-resolved.yaml --flavor m2.large --image f3799482-e852-4a77-9383-408370b81df6 --key-name lxplus xboinc-ubuntu-build
```

You can monitor the creation process from https://openstack.cern.ch/ or by typing:
```
openstack server show xboinc-ubuntu-build
```
Wait until the state is active or running.


## Configuring the machine

Connect via ssh (login: ubuntu, authentication with rsa key).

```bash
ssh ubuntu@xboinc-ubuntu-build
```

Update and test apt install and graphical interface:
```bash
sudo apt update
sudo apt upgrade
sudo apt install gedit
gedit
```

Add admin user:
```bash 
sudo adduser giadarol
sudo usermod -aG sudo giadarol
```

## Enable password authentication

This is done by editing a configuration file:
```bash
sudo vim /etc/ssh/sshd_config
```

Inside the file set:
```
PasswordAuthentication yes
```

Restart ssh:
```bash
sudo systemctl restart sshd
```

## Install graphical interface


Install graphical interface (based on [this](https://phoenixnap.com/kb/how-to-install-a-gui-on-ubuntu) guide):
```
sudo apt install slim # select slim
sudo apt install ubuntu-desktop
sudo reboot
```

The console on openstack restarts in graphical mode