# Beam optics analysis tools

This a list of packages with different stage of maturity and support that cover specific use cases and make use of core ABP software.


### Maintained "Team" Tools

 - **pymask** (G. Sterbini and G. Iadarola): [GitHub](https://github.com/lhcopt/lhcmask/)
      - Main tool to setup LHC and HL-LHC simulations for dynamic aperture with beam-beam effects and field imperfections
      - Contains:
           - Analysis of deferred expressions (identification of dependencies)
           - MadPoint: obtain absolute coordinates combining survey and twiss results
           - Note: some of these functionalities are also available in [pyoptics](#pyoptics)

 - **pockpy** (D. Gamba): [GitHub](https://github.com/pylhc/pockpy)
      - Used to carry out orbit correctors budget calculation for HL-LHC triplet 
      - Note: similar to CORR in MADX but it computes matrices from twiss parameters and computes expected min/max/rms orbit excursions/corrector strength, using linear algebra and non-linear optimizations.

- **xdeps** (R. De Maria): [GitHub](https://github.com/xsuite/xdeps)
      - Generic Python data dependency manager (updates values in containers when setting depending values). It supportss nested structures and multiple input/output. Works on top of any Python data structure.
      - MAD-X expression parser using LARK and compatible with xdeps expressions
      - Simple mad-like environment obtained by damping data from a cpymad instance

- **tfs-pandas** (F. Soubelet, J. Dilly, OMC Team - "officially supported"): [GitHub](https://github.com/pylhc/tfs) | [PyPI](https://pypi.org/project/tfs-pandas/) | [Conda-Forge](https://anaconda.org/conda-forge/tfs-pandas)
      - Pythonic, robust, tested *TFS* files I/O to tailored `pandas` dataframes (`TfsDataFrames`)


### Frequency Analysis Tools

- **PySUSSIX** (S. Joly): [GitHub](https://github.com/PyCOMPLETE/PySUSSIX)
      - Wrapper of SUSSIX.

- **NAFFlib** (S. Kostouglou, K. Paraschou):  [GitHub](https://github.com/PyCOMPLETE/NAFFlib)
      - Tune determination from turn by turn data in C.

- **harpy original** (L. Malina):
      - Harmonic  analysis using quadratic interpolation like SUSSIX.

- **harpy new** (L. Malina):
      - Decompose modes using SVD and zero padding.

- **harmonic_fit.py** (R. De Maria): [GitHub](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/harmonic_fit.py) as part of pyoptics
      - Decomposition using numpy (zero padding, peak finder, lst square fitting), support coupled signals.


### Data format tools

- **tfsdata** (R. De Maria): [GitHub](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/tfsdata/tfsdata.py)  as part of pyoptics
      - Convert tfs file to dictionary  and back

- **sdds** (J. Dilly, F. Soubelet, OMC Team): [GitHub](https://github.com/pylhc/sdds) | [PyPI](https://pypi.org/project/sdds/) | [Conda-Forge](https://anaconda.org/conda-forge/sdds)
      - Python interface for *SDDS* files I/O

- **sddsdata** (R. De Maria): [GitHub](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/sddsdata.py)  as part of pyoptics
      - Convert sdds file to dictionary in pyoptics
      - Forked in SPSMeasurement tools (not really used anymore)

### Job submissions tools

- **pylhc-submitter** (M. Hofer, J. Dilly, OMC Team): [GitHub](https://github.com/pylhc/submitter) | [PyPI](https://pypi.org/project/pylhc-submitter/) | [Conda-Forge](https://anaconda.org/conda-forge/pylhc_submitter) | [Demo](https://slides.com/fsoubelet/pylhc-submitter-presentation/fullscreen)
    - Utility HTCondor Submitter for Parametrized Studies (any language / executable) <br> &rArr; simple to use, positive feed-back from non-OMC users
    - Includes also a wrapper for SixDesk (AutoSix)


### LHC specific tools

- **PyLHC** (J. Dilly, M. Hofer, F. Soubelet, OMC Team): [GitHub](https://github.com/pylhc/PyLHC) | [PyPI](https://pypi.org/project/pylhc/)
    - Tools for particle accelerator data analysis and machine information at CERN
        - BSRT analysis (LHC)
        - (forced) DA analysis
        - BPM calibration calculation (LHC)
        - KickGroups information retrieval (LHC)
        - LSA knobs to MAD-X expressions converter
        - Machine information querying tools (BP, knobs etc)
        - &rArr; The latter two can be nicely combined to recreate knobs and trims from certain dates in MAD-X (Josch has a script <3)
        Note: LSA knobs extraction to MAD-X and optics models is also in [acc-models-lhc/scenarios](https://gitlab.cern.ch/acc-models/acc-models-lhc/-/blob/2022/scenarios)

- **cpymad_lhc** (J. Dilly): [GitHub](https://github.com/JoschD/cpymad_lhc) | [PyPI](https://pypi.org/project/cpymad_lhc/) | [Doc](https://joschd.github.io/cpymad_lhc/)
    - Some tools I use with cpymad:
        - Set at check LHC/HL-LHC corrector limits
        - Closest Tune Approach based coupling correction (fine-tune-coupling but pythonic)
        - Tune and Chroma Matching [but pythonic, similar to Felix, with updated knob-names handling (`_sq`, `_op`)]
        - Orbit Setup (incl. 2022 optics)
        - Set error orders (a4, b5 etc.) automatically
        - Some IO: 
            - get tfs-dataframes with headers from cpymad
            - read/write PTC output
            - SixTrack ouput

- **turn_by_turn** (F. Soubelet, J. Dilly, OMC Team): [GitHub](https://github.com/pylhc/turn_by_turn) | [PyPI](https://pypi.org/project/turn-by-turn/) | [Conda-Forge](https://anaconda.org/conda-forge/turn_by_turn)
     - Pythonic I/O functionality for turn-by-turn BPM measurements data from different particle accelerators


### Machine agnonist tools

- **optics_functions** (J. Dilly, OMC Team): [GitHub](https://github.com/pylhc/optics_functions) | [PyPI](https://pypi.org/project/optics-functions/) | [Conda-Forge](https://anaconda.org/conda-forge/optics_functions)
      - Calculate various beam optics functions from TWISS outputs (`TfsDataframes`)
      - Note: complementary to pyoptics optics class

- **IRNL RDT Correction** (J. Dilly, OMC Team): [GitHub](https://github.com/pylhc/irnl_rdt_correction) | [PyPI](https://pypi.org/project/irnl_rdt_correction/)
    - Calculate local corrections in the IR based on minimizing RDTs
    - Like S. Fartoukh's fortran script, but
        - in python
        - flexible inputs (RDTs, optics, elements)
        - can take feed-down into account
        - can take multiple optics (e.g. both Beams) into account
    - HINT: currently being restructured (in [restructuring branch](https://github.com/pylhc/irnl_rdt_correction/tree/restructuring), which will be version 1.0).
            Code restructuring is done, but an additional pdf-note with background info is currently being written.
       
 - **gmtoolbox** (D. Gamba): [GitLab](https://gitlab.cern.ch/abpcomputing/sandbox/gmtoolbox)
     - small toolbox *under SLOW development* to study impact of misalignment/ground motion on closed orbit
     - fit of single sources of orbit kick (e.g. for 10 Hz oscillation)
     - small function to get data from post mortem

- **pyoptics<div id="pyoptics"></div>** (R. De Maria): [GitHub](https://github.com/rdemaria/pyoptics)
     - generic table manager with specific twiss/track/survey post-prossinging [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/optics.py)
          - row filtering on regexp and column expression, new column on expressions
          - plots with lattices (automatic reload on changes)
          - table pretty printer
          - twiss/survey (interpolation with s, cycle w/wo reset cumulative quantity, range extraction, append)
          - twiss:
              - calculate beta max inside quadrupole, integral k*beta, normalized dispersion, transfer matrix in between points, 2D normalization matrix, partial integrals to compute Q', Q''
              - add errors from error tables
              - compute driving terms kernels
              - compute detuning factor
              - compute footprint based on detuning equations
          - survey (redefine origin to any point (2D only), compute rotation matrix)
     - tfs reader/basic writer [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/tfsdata/tfsdata.py)
     - beam envelope class [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/aperture.py)
          - draw 2d section of beam-envelopes and apertures x-y, s-x, s-y around reference orbit or lab frame
          - compute aperture margin
          - plot beam-beam separations
     - madx language parser [src](https://github.com/rdemaria/pyoptics/tree/master/pyoptics/madlang/madlang.py)
          - parse MAD-X definitions (no support for macros) and build data structure including expressions
          - compute dependency graph
     - harmonic fit [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/harmonic_fit.py)
          - various tune fitting routines on turn-by-turn data
     - optics transition tables (table of strength) [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/irplots.py)
        - plot, polynomial fit with consraints on deritives, convert fit to madx expressions
     - jacobian matching method in python [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/jacobian.py)
     - LHC circuit model [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/lhccircuit.py)
          - compute max ktoI and ItoK, max V, I', I'' using data from LSA or from IREF/VREF fit
          - compute minimum beam-process time base on I', I'' max
     - sdds reader [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/sddsdata.py)
     - yasp reader [src](https://github.com/rdemaria/pyoptics/blob/master/pyoptics/yaspdata.py)
     - lot of obselete files ;-)

- **pyhdtoolkit** (F. Soubelet): [GitHub](https://github.com/fsoubelet/PyhDToolkit) | [PyPI](https://pypi.org/project/pyhdtoolkit/) | [Doc](https://fsoubelet.github.io/PyhDToolkit/) | [Gallery](https://fsoubelet.github.io/PyhDToolkit/gallery/index.html)
    - Pythonic & consistent API for convenience interface to most MAD-X functionality via `cpymad` ([`cpymadtools` submodule](https://github.com/fsoubelet/PyhDToolkit/tree/master/pyhdtoolkit/cpymadtools))
        - Betatron coupling (one-function RDTs calculations, Cminus calculation, CTA routine, coupling correction)
        - Errors assignment utilities
        - Maaaany LHC specific (sometimes very personal) utilities and setup functions (elements installation, knobs, K-mod, beams creation, most usual MAD-X toolbox routines etc)
        - Matching routines for tunes, chroma (similar to Josch)
        - Orbit handling
        - Beam attributes querying
        - PTC routines (RDTs calculation, tracking)
        - Tracking routines
        - Tune utilities (dynap, footprint calculation)
        - TWISS routines utilities
        - Misc utilities (tables querying & export)
    - Consistent plotter functions and utilities ([`plotting` submodule](https://github.com/fsoubelet/PyhDToolkit/tree/master/pyhdtoolkit/plotting))
        - Aperture plotting
        - LHC crossing schemes plotting
        - Beam enveloppe plotting
        - Courant-Snyder phase space plotting
        - Tune diagram plotting
        - Segment-by-segment plotting
        - Public API utilities for most plotters
    - Other mostly personal utilities for various things I did in my work (statistical fitting, htcondor monitoring, context managers, logging, mundane python operations etc)


- **acc_lib** (Elias Waagaard) [GitLab](https://gitlab.cern.ch/elwaagaa/acc_lib)
    - Plotting tools for injection, machine aperture, beam envelopes, phase space diagrams, optics functions
    - Cpymad methods to calculate various quantities 
    - Tune footprints from and simple numpy FFT - also with resonance lines


### Actions
-   (Felix Soubelet, Joshua Dilly, Riccardo De Maria, Elias Waagaard, Davide Gamba, Laurent Deniau)
try to repackage functionalities in one single package with dependencies by:
     - remove duplicates
     - extend existing APIs or create new operations
     - split big packages in small modules or packages
     - write examples for the use cases 
     - Work in progress in  https://github.com/beamopticsanalysis/
         - python package https://github.com/beamopticsanalysis/boa
         - set up https://boa.readthedocs.io/en/latest/
         - next steps: examples

